package logica;

public class Mujer extends Persona {
    public Mujer(String nombre, double peso, double altura, int edad) {
        super(nombre, peso, altura, edad);
    }

    @Override
    public double calcularTMB() {
        return 447.593 + (9.247 * getPeso()) + (3.098 * getAltura()) - (4.33 * getEdad());
    }
}