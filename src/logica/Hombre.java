package logica;

public class Hombre extends Persona{
	public Hombre(String nombre, double peso, double altura, int edad) {
        super(nombre, peso, altura, edad);
        
    }
	@Override
	public double calcularTMB() {
        return 88.362 + (13.397 * getPeso()) + (4.799 * getAltura()) - (5.677 * getEdad());

}
}
