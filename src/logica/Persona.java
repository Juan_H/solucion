package logica;

public abstract class Persona {
	    private String nombre;
	    private double peso;
	    private double altura;
	    private int edad;
		
	    public Persona(String nombre, double peso, double altura, int edad) {
	    	this.nombre=nombre;
	    	this.peso=peso;
	    	this.altura=altura;
	    	this.edad=edad;
			
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public double getPeso() {
			return peso;
		}
		public void setPeso(double peso) {
			this.peso = peso;
		}
		public int getEdad() {
			return edad;
		}
		public void setEdad(int edad) {
			this.edad = edad;
		}
		public double getAltura() {
			return altura;
		}
		public void setAltura(double altura) {
			this.altura = altura;
		}
		public abstract double calcularTMB();
			
		
	    


}
