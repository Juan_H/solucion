package presentacion;
import logica.Persona;
import logica.Mujer;
import logica.Hombre;

public class Mostrar {
	public static void main(String[] args) {
		Hombre H;
		H = new Hombre("juan",70,125,30);
		
		Mujer M;
		M= new Mujer("maria",70,125,30);
		
		System.out.println("TMB de " + H.getNombre() + ": " + H.calcularTMB());
        System.out.println("TMB de " + M.getNombre() + ": " + M.calcularTMB());
		
	}
	}
